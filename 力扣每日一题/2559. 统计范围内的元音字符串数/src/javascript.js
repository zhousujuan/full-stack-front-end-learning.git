/**
 * @param {string[]} words
 * @param {number[][]} queries
 * @return {number[]}
 */
 var vowelStrings = function(words, queries) {
    let n = words.length;
    let prefixSums = new Array(n + 1).fill(0);
    for (let i = 0; i < n; i++) {
        let value = isVowelString(words[i]) ? 1 : 0;
        prefixSums[i + 1] = prefixSums[i] + value;
    }
    let ans = [];
    for (let i = 0; i < queries.length; i++) {
        let start = queries[i][0], end = queries[i][1];
        ans.push(prefixSums[end + 1] - prefixSums[start]);
    }
    return ans;
}

function isVowelString(word) {
    return isVowelLetter(word[0]) && isVowelLetter(word[word.length - 1]);
}

function isVowelLetter(c) {
    return /[aeiou]/.test(c)
}

/**
 * @desc 其他
 * 这个地方要是通过暴力破解的话，也不是不可以解决，但是会存在一个超时的情况，超时的话，就会降低性能
 */

/**
 * @param {string[]} words
 * @param {number[][]} queries
 * @return {number[]}
 */
//  var vowelStrings = function(words, queries) {
//     let arr = [];
//     queries.map(item => {
//         let tmp = 0;
//         for(let i = item[0]; i <= item[1]; i ++) {
//             if(isVowelStrings(words[i])) {
//                 tmp ++;
//             }
//         } 
//         arr.push(tmp)
//     })
//     return arr;
// };

// isVowelStrings = (data) => {
//     let tmp = 'aeiou';
//     if(tmp.indexOf(data[0]) !== -1 && tmp.indexOf(data[data.length -1]) !== -1) {
//         return true;
//     } else {
//         return false;
//     }
// }